// tham số ~ parameter

// hàm không có tham số
function greeting() {
  // định nghĩa function
  console.log("chào bạn");
  console.log("have a good day");
}
// thực thi hàm ~ gọi hàm ~ execute
greeting();
greeting();
greeting();
greeting();
greeting();

// hàm có tham số

function greetingByUsername(username) {
  console.log("Chào " + username);
}

greetingByUsername("Alice");
greetingByUsername("Bob");

var tinhDTB = function (diemToan, diemLy, username) {
  var dtb = (diemToan + diemLy) / 2;

  var content = `Chào ${username}, điểm trung bình của bạn là ${dtb}`;
  // return;
  console.log("content: ", content);
  return dtb;
};

var tdb1 = tinhDTB(2, 4, "Alice");
console.log("tdb1: ", tdb1);
