const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_Black = "uberBlack";

function tinhGiaTienKmDauTien(loaiXe) {
  switch (loaiXe) {
    case UBER_CAR: {
      return 8000;
    }
    case UBER_SUV: {
      return 9000;
    }
    case UBER_Black: {
      return 10000;
    }
  }
}

function tinhGiaTienKm1Den19(loaiXe) {
  if (loaiXe == UBER_CAR) {
    return 7500;
  } else if (loaiXe == UBER_SUV) {
    return 8500;
  } else {
    return 9500;
  }
}

function tinhGiaTienKm19TroDi(loaiXe) {
  if (loaiXe == UBER_CAR) {
    return 7000;
  } else if (loaiXe == UBER_SUV) {
    return 8000;
  } else {
    return 9000;
  }
}
function tinhTienUber() {
  // kiểm tra user đã chọn loại xe hay chưa, nếu chưa thì dừng chương trình
  if (document.querySelector('input[name="selector"]:checked') == null) {
    return;
  }
  var carValue = document.querySelector('input[name="selector"]:checked').value;
  var soKm = document.getElementById("txt-km").value * 1;

  var giaTienKmDauTien = tinhGiaTienKmDauTien(carValue);
  var giaTienKm1Den19 = tinhGiaTienKm1Den19(carValue);
  var giaTienKm19TroDi = tinhGiaTienKm19TroDi(carValue);

  var result;

  if (soKm <= 1) {
    result = soKm * giaTienKmDauTien;
  } else if (soKm > 1 && soKm <= 19) {
    result = 1 * giaTienKmDauTien + (soKm - 1) * giaTienKm1Den19;
  } else {
    result =
      giaTienKmDauTien + 18 * giaTienKm1Den19 + (soKm - 19) * giaTienKm19TroDi;
  }
  console.log("result: ", result);

  document.getElementById("divThanhTien").style.display = "block";
  document.getElementById(
    "divThanhTien"
  ).innerText = `Thành tiền: ${result} VND`;
}

// uber black 18.5km
